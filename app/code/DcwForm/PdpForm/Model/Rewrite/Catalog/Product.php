<?php
    /**
     * Hello Catalog Product Rewrite Model
     *
     * @category    Webkul
     * @package     Webkul_Hello
     * @author      Webkul Software Private Limited
     *
     */
    namespace DcwForm\PdpForm\Model\Rewrite\Catalog;
 
    class Product extends \Magento\Catalog\Model\Product
    {
        public function isSalable()
        {
            // Do your stuff here
            return parent::isSalable();
        }
 
    }

    ?>