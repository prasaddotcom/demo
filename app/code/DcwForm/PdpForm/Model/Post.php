<?php
namespace DcwForm\PdpForm\Model;
class Post extends \Magento\Framework\Model\AbstractModel
{

	protected function _construct()
	{
		$this->_init('DcwForm\PdpForm\Model\ResourceModel\Post');
	}

	// public function getIdentities()
	// {
	// 	return [self::CACHE_TAG . '_' . $this->getId()];
	// }

	// public function getDefaultValues()
	// {
	// 	$values = [];

	// 	return $values;
	// }
}