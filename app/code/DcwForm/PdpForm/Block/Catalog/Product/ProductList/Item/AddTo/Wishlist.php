<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace DcwForm\PdpForm\Block\Catalog\Product\ProductList\Item\AddTo;
use Magento\Framework\View\Element\Template;
use Magento\Wishlist\Helper\Data;
use Magento\Catalog\Block\Product\ProductList\Item\Block;
/**
 * Add product to wishlist
 *
 * @api
 * @since 100.1.1
 */
class Wishlist extends Template
{
    protected $_wishlist;
	public function __construct(
    	\Magento\Wishlist\Model\Wishlist $wishlist,
    \Magento\Framework\View\Element\Template\Context $context,
    Data $helperData,
    Block $productBlock,
    array $data = []
		)
	{
        $this->_wishlist = $wishlist;
        $this->_wishlistHelper = $helperData;
        $this->product = $productBlock;
    parent::__construct($context, $data);
	}
    protected function _isCustomerLogIn()
    {
        return $this->_wishlistHelper->_customerSession->isLoggedIn();
    }

    public function isProductInWishlist($productId)
     {
        if($this->_isCustomerLogIn()){
            $customerId =                         $this->_wishlistHelper->_customerSession->getId();
            $wishlist_collection = $this->_wishlist->loadByCustomerId($customerId, true)->getItemCollection();

            foreach($wishlist_collection->getData() as $wishlist){
                if($productId == $wishlist['product_id']){
                    return true;
                }
            }
        }
      return false;                  
     }


    /**
     * @return \Magento\Wishlist\Helper\Data
     * @since 100.1.1
     */
    public function getWishlistHelper()
    {
        
        return $this->_wishlistHelper;
    }
    public function getProduct()
    {
        return $this->product->getProduct();
    }
   
    public function sayHello()
    {
        return 34534456;
    }

    // public function getMyCustomMethod()
    // {
    //     return '<b>I Am From MyCustomMethodsss</b>';
    // }

    
}
