<?php
    /**
     * Hello Rewrite Product View Controller
     *
     * @category    Webkul
     * @package     Webkul_Hello
     * @author      Webkul Software Private Limited
     *
     */
    namespace DcwForm\PdpForm\Controller\Index\Rewrite\Product;
 
    class View extends \Magento\Catalog\Controller\Product\View
    {
        /**
         * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
         */
        public function execute()
        {
            // Do your stuff here
            
            return parent::execute();
        }
    }

    ?>