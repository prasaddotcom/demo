<?php

namespace DcwForm\PdpForm\Controller\Index;

class DisplayText extends \Magento\Framework\App\Action\Action
{

	public function execute()
	{
		$textDisplay = new \Magento\Framework\DataObject(array('text' => 'Mageplaza'));
		$this->_eventManager->dispatch('dcw_display_text', ['mp_text' => $textDisplay]);
        echo $textDisplay->getText();
		exit;
	}
}
?>