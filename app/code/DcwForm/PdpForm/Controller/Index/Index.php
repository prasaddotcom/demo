<?php
namespace DcwForm\PdpForm\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	protected $_postFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\DcwForm\PdpForm\Model\PostFactory $postFactory
		)
	{
		$this->_pageFactory = $pageFactory;
		$this->_postFactory = $postFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
        
        $post = $this->_postFactory->create();
        
        $postValue = $this->getRequest()->getParams();

        try {
            //create a post object and set data to the object
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($postValue);

            $error = false;

            //Zend validation for checking if post varibales are not empty
            if (!\Zend_Validate::is($postObject->getName(), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is($postObject->getEmail(), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is($postObject->getPassword(), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is($postObject->getGender(), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is($postObject->getPhone(), 'NotEmpty')) {
                $error = true;
            }


            if ($error) {
                //throw exception if any post variable is empty
                throw new \Exception('All the fields are Mandatory');
            }
            //setting data to the preorder model
            $post->setName($postValue['name']);
            $post->setEmail($postValue['email']);
            $post->setPassword($postValue['password']);
            $post->setGender($postValue['gender']);
            $post->setPhone($postValue['phone']);
            

            //Save the data in the table
            $post->save();

            //getting referer url
            $_redirectUrl = $this->_redirect->getRefererUrl();

            //redirecting to the referer page
            $this->_redirect($_redirectUrl);
            //shows Message that preorder data is saved
            $this->messageManager->addSuccess(__('Your Preorder data have been saved Successfully'));
            return;
        } catch (\Exception $e) {
            //Shows Message when error occurs
            $this->messageManager->addError(__($e->getMessage()));
            //getting referer url
            $_redirectUrl = $this->_redirect->getRefererUrl();
            //redirecting to the referer page
            $this->_redirect($_redirectUrl);
            return;
        }
	}
}