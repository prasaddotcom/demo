<?php

namespace DcwForm\PdpForm\Controller\Adminhtml\Post;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use DcwForm\PdpForm\Model\ResourceModel\Post\CollectionFactory;
use DcwForm\PdpForm\Model\PostFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\ResponseInterface;

class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory, PostFactory $postFactory)
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->postObject = $postFactory;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
       $ids =  $this->getRequest()->getParams();
    //    if(isset($ids['selected'])){
    //     $collection =$this->collectionFactory->create()->addFieldToFilter('post_id', array(
    //         'in' => $ids['selected'])
    //         );;
    //     $collectionSize = $collection->getSize();
    //    }else{
    //     $this->messageManager->addSuccess(__('Please select atleast one'));
    //     $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
    //     return $resultRedirect->setPath('*/*/');   
    // }
       
        $count = 0;
        foreach ($ids['selected'] as $id) {
           $item = $this->postObject->create()->load($id);
            $item->delete();
            $count++;
        }

        $this->messageManager->addSuccess(__('A total of %1 element(s) have been deleted.', $count));

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }
}