<?php
/**
 * Sample_News extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category  Sample
 * @package   Sample_News
 * @copyright 2016 Marius Strajeru
 * @license   http://opensource.org/licenses/mit-license.php MIT License
 * @author    Marius Strajeru
 */
namespace DcwForm\PdpForm\Controller\Adminhtml\Post;
// namespace DcwForm\PdpForm\Block\Adminhtml\Contact\Edit\GenericButton;
use Magento\Backend\App\Action;
class Save extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;

	public function __construct(
		\Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \DcwForm\PdpForm\Model\PostFactory $postFactory ,
        \Magento\Framework\Stdlib\DateTime\DateTime $date

	)
	{
		parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_postFactory = $postFactory;
       $this->date = $date;
	}
    public function execute()
    {
    
        $data = $this->getRequest()->getPostValue();
        try{
        // var_dump($data['contact']['post_id']); die('abcd');
        $post = $this->_postFactory->create()->load($data['contact']['post_id']);
        $post->setName($data['contact']['name']);
        $post->setEmail($data['contact']['email']);
        $post->setPassword($data['contact']['password']);
        $post->setGender($data['contact']['gender']);
        $post->setPhone($data['contact']['phone']);
        $date = $this->date->gmtDate();
        // var_dump($date);die('asd');
        //$post->setUpdatedAt($date['contact']['updated_at']);

        $post->setUpdatedAt($date);



        

        //Save the data in the table
        $post->save();

        //getting referer url
        $_redirectUrl = $this->_redirect->getRefererUrl();
        $this->_redirect('*/*/index');
        //shows Message that preorder data is saved
        $this->messageManager->addSuccess(__('Your Preorder data have been saved Successfully'));
        return;
        }catch(\Exception $e) {
            //Shows Message when error occurs
            $this->messageManager->addError(__($e->getMessage()));
            //getting referer url
            $_redirectUrl = $this->_redirect->getRefererUrl();
            //redirecting to the referer page
            $this->_redirect($_redirectUrl);
            return;
        }
       
        
    }
    
   
}