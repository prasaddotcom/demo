<?php

namespace Dcw\Mymodule\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Booking extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
        $post = (array) $this->getRequest()->getPost();
        if($post){
            print_r($post);
        }
        
        $this->_view->loadLayout();
        $this->_view->renderLayout();
	}
}